#include <stdio.h>
#include <string.h>
#include <math.h>
#include "convert.h"

/*
Trabalho de:
 - Felipe Jangelavicin
 - Vinicius Miguel
 - Luan Rivelo Tosta
*/

int main()
{
    int resp, numDec;
    char opcao, numBin[50];

    do
    {
        printf("\n\n1 - Decimal para binario");
        printf("\n2 - Binario para decimal");
        printf("\nEscolha uma opcao: ");
        scanf("%d", &resp);

        switch(resp)
        {
            case 1:
                printf("\nInforme um valor decimal: ");
                scanf("%d", &numDec);

                dec2bin(numDec);

                break;
            case 2:
                printf("\nInforme um valor binario: ");
                setbuf(stdin, NULL);
                gets(numBin);

                bin2dec(numBin);

                break;
            default:
                printf("\nInforme uma opcao valida!");
        }

        printf("\n\nDeseja repetir o programa? (S/N)\n");
        setbuf(stdin, NULL);
        scanf("%c", &opcao);

    }while(opcao=='S'||opcao=='s');
}
