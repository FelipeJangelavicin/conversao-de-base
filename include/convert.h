void dec2bin(int numDec)
{
    int i=0, j, divNum, vetA[50];

    divNum = numDec;

    do
    {
        vetA[i] = divNum % 2;
        divNum = divNum / 2;

        i++;

    }while(divNum>=1);

    printf("Valor em binario: ");

    for(j=i-1;j>=0;j--)
    {
        printf("%d\t", vetA[j]);
    }
}

void bin2dec(char numBin[])
{
    int i, j, k, valDec=0, vetB[50], vetC[50];

    for(i=0;numBin[i]!='\0';i++)
    {
        vetB[i] = (int)numBin[i] - 48;
    }

    for(j=0;j<=i;j++)
    {
        vetC[j] = vetB[i-j];
    }

    for(k=i;k>=0-1;k--)
    {
        if(vetC[k]==1)
        {
            valDec = valDec + pow(2, k);
        }
    }

    printf("\nValor em decimal: %d", valDec/2);
}
