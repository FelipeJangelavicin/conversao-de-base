# Algoritmo para Conversão de Base - Método Divisão Sucessivas

## Descrição


## Como utilizar o CONVERSOR DE BASE

Para usar a biblioteca adicione o `convert.h` no seu código:

```c
#include <stdio.h>
#include "convert.h"
```

Para rodar exemplo, use o seguinte comando:

```bash
gcc ./src/main.c -I ./include/ -o main
./main
```
e para rodar o teste:
```bash
gcc ./tests/unitTest.c -I./include/ -o test_bin2dec
```

## Autores
Esse algoritmo foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11794922/avatar.png?width=400)  | Vinicius Miguel | [@osogvm](https://gitlab.com/osogvm) | [viniciusmiguel@alunos.utfpr.edu.br](mailto:viniciusmiguel@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11520126/avatar.png?width=400)  | Felipe Jangelavicin | [@FelipeJangelavicin](https://gitlab.com/FelipeJangelavicin) | [felipejangelavicin@alunos.utfpr.edu.br](mailto:felipejangelavicin@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8295307/avatar.png?width=400)| Bianca Santos | [@bianca_santos](https://gitlab.com/bianca_santos) | [biancasantos@alunos.utfpr.edu.br](mailto:biancasantos@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/11747532/avatar.png?width=400)| Luan Rivelo Tosta | [@tostadev](https://gitlab.com/tostadev) | [tosta@alunos.utfpr.edu.br](mailto:tosta@alunos.utfpr.edu.br)





